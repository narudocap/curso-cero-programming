## Sudoku

<!--["Sudoku"](images/sudoku.png "Sudoku")-->
<img src="images/sudoku.png"
     alt="Sudoku"
     style="float: right; margin-left: 10px;"
     width="300"/>

Sudoku is a puzzle game composed of a grid of 81 cells arranged in nine rows and nine columns. Besides that, there are nine blocks in the grid, each block being a subgrid of 3x3 cells.

In the initial state of the game, some of the cells are filled with numbers between 1 and 9. The player must fill the other cells, also with numbers between 1 and 9 in such a way that no number is repeated in the same row, the same cell, or the same block. The player cannot change the numbers which where initially set. Sound puzzles have a single solution.

### Representing the game

In Object-Oriented Programming, objects in the real world are represented by objects in the program code. As the main object in this program is the grid, there will be a `SudokuGrid` class in our code.

However, the grid is a complex object, composed of simpler objects, the cells. One of the guidelines when constructing objects and classes is _keeping it simple_. Therefore, another class, `Cell`, will be defined in our code.

Both classes are related: a grid has cells. This kind of strong relationship where one of the parties is a compose and the other one is a component, is called *composition* and is graphically represented by and arrow with a filled diamond in the compose end of the relationship.

<!--["Sudoku"](images/grid-cell-composition.png "Sudoku")-->
<img src="images/grid-cell-composition.png"
     alt="Grid Cell Composition"
     style="float: right; margin-left: 10px;"
     width="150"/>

Objects in an object-oriented program comprises, on the one hand, some data. These data are stored in variables called *attributes*. The set of values of an object's attributes represent the object's state. On the other hand, objects also provide *methods*, which are pieces of code that allow to change the object's state or compute some new values or information from the object's state.

In the case of a cell, it is important to know if the cell is empty or filled and, if it is filled, what is the number stored in the cell. Indeed, two situations must be distinguished. If a cell was initially filled, the player cannot change its value. To represent the possible cell's states we will use a different element, an enumerarion type (also known by the abbreviation *enum*). These types define a set of possible values. In our case, the values will be: *fixed*, *empty*, and *filled*. In this case, it is said that the relationship between the cell and its state is looser.

<!--["Sudoku"](images/grid-cell-state-relationship.png "Sudoku")-->
<img src="images/grid-cell-state-relationship.png"
     alt="Grid Cell State Relationship"
     style="float: right; margin-left: 10px;"
     width="300"/>

In object-oriented programming, objects are not directly defined. Instead, *classes* are used. Classes define the common features of a set of elements, the objects. In our case, the class `Cell` will describe the attributes and methods common to all cells. Every time an object is needed, a new object is created. Objects have their own copy of the attributes of the class, and, thus, can have different values in their copies or, in other words, their own state, independently of the state of the other objects of the same class. Going back to our cell example, some cells in the grid will be empty and some others will hold values. Among the latter, some of them will be set from the beginnig, or fixed, and the rest will have been filled by the user.

> ##### Task 1
> 1. Open Eclipse.
> 2. If there is no active workspace, create one.
> 3. Create a new Java project named `sudoku`. Accept the default options offered by Eclipse.
> 4. In this newly created project, create a new package named `model`. Accept the default options offered by Eclipse.
> 5. Let us install the `ANSI Escapes in Console` plugin. First, open the menu option `Help` -> `Eclipse Marketplace`. Select the tab `Search`. In the `Find` box, type `ANSI Escapes in Console` and press the button `Go`. When the plugin appears, press the button `Install` and follow the instructions. Restart Eclipse.

### Model: `SudokuGrid` and `Cell` classes

`SudokuGrid` and `Cell` are the two main classes of our model.

> ##### Task 2
> 1. Copy the files [`SudokuGrid.java`](files/model/SudokuGrid.java) and [`Cell.java`](files/model/Cell.java) in the package `model`.

The editor will show some errors because the definition of some auxiliary elements are missing. These elements are the enum type `CellState` and the exception classes `EmptyCellException` and `NumberOutOfRangeException`.

> ##### Task 3
> 1. Copy the files [`CellState.java`](files/model/CellState.java), [`EmptyCellException.java`](files/model/EmptyCellException.java) and [`NumberOutOfRangeException.java`](files/model/NumberOutOfRangeException.java) in the package `model`.

The compilation errors will be gone.

We are now building a complete program that makes use of those classes.

> ##### Task 4
> 1. Create a new package called `main`.
> 2. Copy the file [`MainFirstVersion.java`](files/main/MainFirstVersion.java) in that package.

This first program is a very basic version that only creates a grid, populates some cells and shows the grid content in the console. There are some interesting details in this file. The first one is the definition of the function `main`. This function is the program's entry point. That is, this is from where the program starts working. The second one is the creation of an object of the class `SudokuGrid`. This creation is made by calling the instruction `new`. In turn, when the sudoku grid is created, it creates the cells that compose the grid.

The third interesting point are the messages sent to the sudoku grid. Some of those messages change the grid state, either by setting new values or by deleting values previously set. The state of the grid is shown on the console by generating first a character string that represents its state and, then, writing that string on the console.

> ##### Exercises:
> 1. Modify the file `MainFirstVersion.java` by adding and removing numbers from the grid and then showing the grid state on the console. What happens when one of the values - for the number, the row, or the column - are not within their valid limits? What happens when 0 or 9 are used to indicate the row or the column? Why does this happen?  !["greenpepper"](images/greenpepper-25.png "greenpepper")
> 2. Try to modify how the grid is shown on the console, by adding a first row with the column number and the row number at the begining of each file (Option: Write a letter in the interval A-I instead of a number for the row).  !["yellowpepper"](images/yellowpepper-25.png "yellowpepper")

### Controlling the game

The above first version of the program, with such a simple `main` function, does not give a player the chance to play the game. We will add now that possibility. In order to do it, we are using an underlying architecture widely used in programming, the so-called Model-View-Controller architecture. With this architecture, classes are grouped in three layers, according to their main purpose.

The classes shown in the previous section (`SudokuGrid`and `Cell`) lay in the Model part of the architecture, as they represent the data handled in the program. The View layer corresponds to the classes that handle the user interaction, both to pay attention to user input and to render results to the user. The Control layer hosts the set of classes that implements the business logic of the program, that is, how the program reacts to user requests to compute the desired results.

When the user inputs a request, the View layer captures the request and sends it to the Controller layer. In response to the request, the Controller layer reads the Model state and computes an answer for the request, which may perhaps involve changing the Model state. Then, the Controller calls the View to inform that the state has changed and that it must read the new Model state to update the information shown to the user.

<!--["MVC"](images/model-view-controller.png "Sudoku")-->
<img src="images/model-view-controller.png"
     alt="Model View Controller"
     style="float: right; margin-left: 10px;"
     width="300"/>

The controller will be fairly simple, as it is the model and the set of possible actions the player can take. The controller will implement the rules to change a value in the grid: A value set from the begining is considered to be fixed and cannot be changed or erased. And it does not make sense to empty a cell that is already empty.

The view will show the present state of the game to the user and will prompt for an action. When the user enters a choice (whether to set or erase a value and the cell's coordinates), the view will pass it to the controller, that will check its availability and will modify the model, if it has to. The controller will answer with a message if the player's choice was wrong or if the sudoku has been successfully filled.

### Text bases console view

In our case, the first view will be text based. It will read the player's action (update or remove a value and the cell coordinates) from the keyboard, and will show the state of the grid after having applied the user's action on the console.

> ##### Task 5
> 1. Create three new packages named `utils`, `controller` and `view`.
> 2. Copy the files [`PlayerAction.java`](files/utils/PlayerAction.java) and [`PlayerChoice.java`](files/utils/PlayerChoice.java) in the package `utils`.
> 3. Copy the files [`GameView.java`](files/view/GameView.java) and [`ConsoleView.java`](files/view/ConsoleView.java) in the package `view`.
> 4. Copy the file [`GameController.java`](files/controller/GameController.java) in the package `controller`.
> 5. Copy the file [`ConsoleMain.java`](files/main/ConsoleMain.java) in the package `main`.

The `ConsoleMain.java` contains the main function for this version. It creates a new sudoku game by calling the method `SudokuGrid.createNewGame()`. It also creates a new controller and a view that works in text mode on the console.

The method `SudokuGrid.createNewGame()` creates a new game by, first, generating a grid completely filled and, then, erasing cells while the puzzle keeps a single solution.

> ##### Exercises:
> 3. Why, when the view sends the controller the row and column the player has chosen it substracts one to both values? Could you change the code so the user has to enter a letter in the range [A - I] to choose the row? Hint: In Java, characters can be translated into integer numbers through a type cast: `int charAsInt = (int) 'A';`. !["yellowpepper"](images/yellowpepper-25.png "yellowpepper")

> 4. The state of the grid is shown on the console by using the method `toString` of the class `SudokuGrid`. However, this way of rendering the grid is too simple and, for example, does not distinguish between fixed or filled cells. Try to improve how the grid is rendered by using colors. Write a method that prints fixed cells in blue, filled cells with correct values in black and filled cells with a value that was repeated in the same row, column or block in red. Take a look to the method `toString` of the class `SudokuGrid` and copy all the code that you think is useful. In addition, you will have to use the method `cellState` of the class `SudokuGrid`. To change colors, use the so-called ANSI escape codes. Add the following lines to the class `ConsoleView` (right after the line where the class is defined):
```
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_BLACK = "\u001B[30m";
```
To change the color in which the text is printed, print first the escape code for the desired color. After the colored text, use de reset escape code to go back to the default color.

```
	System.out.println(ANSI_RED + "Text in red" + ANSI_RESET)
```
!["redpepper"](images/redpepper-25.png "redpepper")


### Model revisited: `SudokuGrid` class methods

The class `SudokuGrid` provides a set of public methods to change and read the states and values of the cells (`setValueAt`, `fixValueAt`, `getValueAt`, `emptyCell`, and `cellState`). Those are simple methods that set or get attributes of the corresponding cell, with added checkings to make sure that only valid coordinates are provided.

Other methods are used to check whether the cells filled in a grid make a valid puzzle or not. The auxiliary method `timesInSector` computes how many times a value is repeated in a part of the grid. Depending on the values passed as parameters, it can be used to look for repeated values in a row, in a column, or in a block. It is used by the method `canFit`, that checks whether a value is valid in a certain cell or it is repeated. It is also used by the method `isValid` that checks whether all the cells are valid. On the other hand, the method `isFull` computes whether the grid is completely filled or not.

> ##### Task 6
> 1. Copy the file [`MainRevisitedModel.java`](files/main/MainRevisitedModel.java) in package `main`.

Class `MainRevisitedModel` has the `main` function for this version of the program. First, it creates a new sudoku using the constructor `SudokuGrid()`. Then, it performs several actions on it: (1) it sets two valid values at different cells, prints the sudoku, and prints a message indicating whether it is valid or not; (2) it sets an additional (incorrect) value, prints the sudoku, and rechecks the validity of the sudoku; (3) prints whether the sudoku is full or not; (4) it fills
the sudoku and rechecks whether it is full or not.

> ##### Exercises:
> 4. Avoiding duplication of code. Inspecting the code of methods `canFit` and `isValid` in the `SudokuGrid` class, some duplicated code can be seen. Duplication of code must be avoided whenever possible. Modify the code to get rid of that duplicated code. Hint: A grid is valid is all the filled cells can fit. Run `MainRevisitedModel.java` to check how it works. !["yellowpepper"](images/yellowpepper-25.png "yellowpepper")
>
> 5. Counting empty cells. The method `isFull` in the `SudokuGrid` class computes whether a grid is full or not by looking for an empty cell. Write an alternative version of the method that counts how many cells are not empty (fixed or filled) and compare that number to the total number of cells in a grid. Run `MainRevisitedModel.java` to check how it works. !["yellowpepper"](images/yellowpepper-25.png "yellowpepper")

The methods `fillSudokuGrid` and `fillSudokuGridRec` are used to complete an empty or a partially filled grid. The former method calls to the latter one to compute the result. `fillSudokuGridRec` uses *recursion* (a problem solving technique) to solve the problem by solving simpler problems and the composing the solution. To fill the grid the method advances row by row, and column by column, trying to set a valid value in the empty cells that it finds. If the proposed value is valid, the search continues. If it is not, the next value is used. If no valid values are found, the method takes a step back to the previous cell with another candidate value.

The present code produces always the same grid, because it always start with the list of candidates {1, 2, 3, 4, 5, 6, 7, 8, 9}.

> ##### Exercises:
> 6. The list of candidates is initialized in the method `fillSudokuGridRec`, in the `else` branch below the comment `// Position (x, y) is empty`. Change the line that initializes the list of candidates by the line below it, that is commented. That line calls a method that produces different lists everytime it is called. Run `MainRevisitedModel.java` several times to check that the changes you did work. !["greenpepper"](images/greenpepper-25.png "greenpepper")

Finally, the method `createNewGame` returns a new partially filled sudoku with a single solution. The approach to do it is, first, compute a completely filled sudoku grid and, then, remove some elements one by one while the remaining grid has a single solution. Finally, the remaining cells are marked as *fixed*.

> ##### Exercises:
> 7. It would be interesting to initalize a sudoku grid from a previously created sample. Write a method that initializes a sudoku grid from a two dimensional array like the next one, where 0's mean empty cells.  !["yellowpepper"](images/yellowpepper-25.png "Sudoku")
> ~~~
> int gridContents [][] = {
> 				{0,6,0,1,0,4,0,5,0},
> 				{0,0,8,3,0,5,6,0,0},
> 				{2,0,0,0,0,0,0,0,1},
> 				{8,0,0,4,0,7,0,0,6},
> 				{0,0,6,0,0,0,3,0,0},
> 				{7,0,0,9,0,1,0,0,4},
> 				{5,0,0,0,0,0,0,0,2},
> 				{0,0,7,2,0,6,9,0,0},
> 				{0,4,0,5,0,8,0,7,0}
> 			};
> ~~~
> 8. Finding *Naked Singles*. A cell is called a *naked single* if there is only one candidate value remaining for it, because the rest of candidates are already present in the row, the cell or the block. Add a method to the class `SudokuGrid` that returns the only candidate value for a cell if it is a naked single, or 0 otherwise. Hint: The method `canFit` can be used to compute this result. !["yellowpepper"](images/yellowpepper-25.png "yellowpepper")
>
> 9. Finding *Full Houses*. A cell is called a *full house* if it is the only empty cell in its row, its column, or its block. Add a method to the class `SudokuGrid` that returns `true` if the cell is a full house, and `false` otherwise. !["yellowpepper"](images/redpepper-25.png "yellowpepper")

### Graphical View

We are adding now an alternative view to the program, a graphical one where the sudoku is presented in a window and the player can use the mouse to select the cell to be edited. It looks like the first picture in this document.

<!--["SudokuGraphicElements"](images/sudokugraphicelements.png "Sudoku")-->
<img src="images/sudokugraphicelements.png"
     alt="Sudoku"
     style="float: right; margin-left: 10px;"
     width="300"/>

In the pictures, fixed cells are shown in blue, filled cells with values that did not clash with other values previously set are shown in black, and filled cells with values that clashed with other values previously set are shown in red. The active cell is highlight by means of a grey rectangle.

The graphic elements are highlighted by coloured rectangles. The red rectangle marks the outmost graphic element, the *Frame*. The frame is coded in the `GrameFrame` class. It contains a header bar, with the program name (`Sudoku`), and three buttons on the left, to close, minimize and maximize the window, respectively. The bottom part is the *content panel*, shown within the green square. This panel, coded in the class `GridPanel`, corresponds to the concept of `SudokuGrid` in the model layer. Whithin the grid panel, we can find 9 block panels, the center one highlighted by the yellow square. This elements are coded in the class `BlockPanel`. This class has not a direct counterpart in the model layer, it is a convenience class to make easier to draw the thick lines that separates blocks. And the inner element is another panel, in this case a panel for each cell, one of them highlighted by the sky blue square. This panel is coded in the class `CellPanel` and has a direct correspondance with the `Cell` class in the model layer. Cell panes contains a text field where the cell values are written.

> ##### Task 6
> 1. Copy the files [`BlockPanel.java`](files/view/BlockPanel.java), [`CellPanel.java`](files/view/CellPanel.java), [`GameFrame.java`](files/view/GameFrame.java), and [`GridPanel.java`](files/view/GridPanel.java) in the package `view`.
> 2. Copy the file [`SudokuGraphicMain.java`](files/main/SudokuGraphicMain.java) in the package `main`.
> 3. Run the file [`SudokuGraphicMain.java`](files/main/SudokuGraphicMain.java).

### Listening to the player's actions

In our graphic view, cells are the elements that reacts to the player's actions. They are said to be *listeners*, as they are waiting for an action to happen, and they respond to it.

Two different types of actions are defined in the cells, one for the mouse, to select a cell, and another one for the keyboard, to set a new value in the active cell or to delete the present value. The method `addMouseListener` sets this binding to the mouse actions. Its parameter is an instance of a new anonymous class `MouseListener` that defines what actions are to be taken when a mouse input has been detected. The possible inputs from the mouse are that one of the mouse buttons is pressed, released, or clicked (pressed and then released), or that the mouse pointer enters in a graphic element area or exits it.

> ##### Exercises:
> 10. In the present code, the cell reacts when the mouse is clicked. Change the code of the anonymous class that implements the `MouseListener` interface used as parameter of method `addMouseListener` in class `CellPanel` so that the cell reacts to the other possible mouse events and compare how the program behaviour changes. !["yellowpepper"](images/greenpepper-25.png "yellowpepper")

On the other hand, the method `addKeyListener` is used to react to a keystroke. To do it, an instance of an anonymous class that implements `KeyListener` is defined. In this case, the three possible actions are that a key is typed, released, or pressed (typed and then released). If the value the player presses in incorrect, that is, it is not a number between 1 and 9, or the delete or backspace keys, a window pops up to tell the player that the pressed key is not allowed in the game.

> ##### Exercises:
> 11. In the present code, the program creates a popup window with the "Wrong Key" error message when the Alt, Control or Command (in MacOs) keys are pressed. That behaviour is annoying when the player wants to switch to another program by pressing the keys combination Alt+Tab or Command+Tab in MacOs. Change the code of the method `keyPressed` of the anonymous object of class `KeyListener` used as parameter of method `addKeyListener` in class `CellPanel` so that the popup window does not prompt when Alt, Control or Command keys are pressed. Hint: Check the reference for the `KeyEvent` class to find out how to identify those keys [Java KeyEvent reference](https://docs.oracle.com/javase/10/docs/api/java/awt/event/KeyEvent.html). !["redpepper"](images/redpepper-25.png "redpepper")

package controller;

import model.CellState;
import model.SudokuGrid;
import view.GameView;

/**
 * Controller class. It receives messages from the view layer when 
 * the player sets a value in a cell or erases its content.
 * It checks whether the puzzle has been completed or not.
 * @author jmalvarez
 *
 */
public class GameController {

	public static String successMessage = "Congratulations. You have completed the puzzle!";
	public static String cellAlreadyEmpty = "This cell is already empty";
	public static String cellFixed = "The value of this cell is fixed and cannot be changed";


	SudokuGrid gridModel;
	GameView gView;

	/**
	 * Constructor. The controller has access to the model to change and retrieve its state
	 * and to the view to indicate it that the game is over
	 * @param grid
	 * @param view
	 */
	public GameController(SudokuGrid grid, GameView view) {
		gridModel = grid;
		gView = view;
	}

	/**
	 * This method is called when the player types a new value for a cell.
	 * @param num. New value for the cell.
	 * @param row. Cell row.
	 * @param column. Cell column.
	 */
	public void setValueAt (int num, int row, int column) {
		if (gridModel.cellState(row, column) == CellState.FIXED) {
			gView.showMessage(cellFixed);
		} else {
			gridModel.setValueAt(num, row, column);

			if (gridModel.isValid() && gridModel.isFull()) {
				gView.showMessage(successMessage);
			}
		}
	}

	/**
	 * This method is called when the player erases the value from a cell.
	 * @param num. New value for the cell.
	 * @param row. Cell row.
	 * @param column. Cell column.
	 */
	public void emptyCell (int row, int column) {
		if (gridModel.cellState(row, column) == CellState.FIXED) {
			gView.showMessage(cellFixed);
		} else if (gridModel.cellState(row, column) == CellState.EMPTY) {
			gView.showMessage(cellAlreadyEmpty);
		} else {
			gridModel.emptyCell(row, column);
		}
	}
}

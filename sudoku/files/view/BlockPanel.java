package view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import controller.GameController;
import model.SudokuGrid;

/**
 * Graphic element that represents a block in the grid. It is composed of a
 * matrix of cells. It has a reference to the controller and to the model.
 * 
 * @author jmalvarez
 *
 */
public class BlockPanel extends JPanel {

	private static final long serialVersionUID = 5428240341088403556L;
	private static final int GAP = 1;
	private static final Color BG = Color.BLACK;

	private GridPanel containerGridPanel;
	private CellPanel cells[][];

	/**
	 * Constructor. It calls the constructors for the inner cells.
	 * 
	 * @param sudokuGame
	 * @param row
	 * @param column
	 * @param gp
	 */
	public BlockPanel(SudokuGrid sudokuGame, int row, int column, GridPanel gp) {
		containerGridPanel = gp;
		setBackground(BG);
		setBorder(BorderFactory.createEmptyBorder(GAP, GAP, GAP, GAP));
		setLayout(new GridLayout(SudokuGrid.BLOCK_LENGTH, SudokuGrid.BLOCK_LENGTH, GAP, GAP));
		cells = new CellPanel[SudokuGrid.BLOCK_LENGTH][SudokuGrid.BLOCK_LENGTH];
		for (int i = 0; i < SudokuGrid.BLOCK_LENGTH; i++) {
			for (int j = 0; j < SudokuGrid.BLOCK_LENGTH; j++) {
				cells[i][j] = new CellPanel(sudokuGame, row * SudokuGrid.BLOCK_LENGTH + i,
						column * SudokuGrid.BLOCK_LENGTH + j, this);
				add(cells[i][j]);
			}
		}
	}

	/**
	 * This method attaches the controller to the block and to the inner cells.
	 * 
	 * @param gc. Controller to be attached.
	 */
	public void attachController(GameController gc) {
		for (int i = 0; i < SudokuGrid.BLOCK_LENGTH; i++) {
			for (int j = 0; j < SudokuGrid.BLOCK_LENGTH; j++) {
				cells[i][j].attachController(gc);
			}
		}

	}

	/**
	 * This method is called from a newly selected cell. It passes the information
	 * to the enclosing grid.
	 * 
	 * @param selectedCellRow.    Row of the newly selected cell.
	 * @param selectedCellColumn. Column of the newly selected cell.
	 */
	public void newCellSelected(int selectedCellRow, int selectedCellColumn) {
		containerGridPanel.newCellSelected(selectedCellRow, selectedCellColumn);
	}

	/**
	 * This method deselects the previously selected cell when the user selects a
	 * new cell.
	 * 
	 * @param cellRow.    Row of the cell to be deselected.
	 * @param cellColumn. Column of the cell to be deselected.
	 */
	public void deselectCell(int cellRow, int cellColumn) {
		cells[cellRow % SudokuGrid.BLOCK_LENGTH][cellColumn % SudokuGrid.BLOCK_LENGTH].deselect();
	}
}

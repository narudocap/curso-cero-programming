package view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import controller.GameController;
import model.SudokuGrid;

/**
 * Graphic element that represents the sudoku grid. It is composed of a matrix of blocks.
 * It has a reference to the controller and to the model.
 * It recalls which is the last selected cell, so it can be unselected when the user
 * selects a new cell.
 * @author jmalvarez
 *
 */
public class GridPanel extends JPanel{

	private static final long serialVersionUID = -8721058545001746824L;
	static final int BLOCKSPERLINE = SudokuGrid.GRID_LENGTH / SudokuGrid.BLOCK_LENGTH;
	private static final Color BG = Color.BLACK;
	private static final int GAP = 3;
	
	private BlockPanel blocks[][];
	
	private int selectedCellRow;
	private int selectedCellColumn;
	
	/**
	 * Constructor. It calls the constructors for the inner blocks.
	 * @param sudokuGame
	 */
	public GridPanel(SudokuGrid sudokuGame) {	
	    setBackground(BG);
		setLayout(new GridLayout(BLOCKSPERLINE, BLOCKSPERLINE, GAP, GAP));
	    setBorder(BorderFactory.createEmptyBorder(GAP, GAP, GAP, GAP));
		blocks = new BlockPanel[SudokuGrid.BLOCK_LENGTH][SudokuGrid.BLOCK_LENGTH];
		for (int i = 0; i < SudokuGrid.BLOCK_LENGTH;i++) {
			for (int j = 0; j < SudokuGrid.BLOCK_LENGTH;j++) {
				blocks[i][j] = new BlockPanel(sudokuGame, i, j, this);
				add(blocks[i][j]);
			}
		}
		selectedCellRow = -1;
		selectedCellColumn = -1;

	}

	/**
	 * This method is used to attach the controller, so it can reported
	 * about the user's actions.
	 * @param gc
	 */
	public void attachController(GameController gc) {
		for (int i = 0; i < SudokuGrid.BLOCK_LENGTH;i++) {
			for (int j = 0; j < SudokuGrid.BLOCK_LENGTH;j++) {
				blocks[i][j].attachController(gc);
			}
		}
	}

	/**
	 * This method updates the cell selected by the user. The previously 
	 * selected cell is unselected.
	 * @param newRow
	 * @param newColumn
	 */
	public void newCellSelected(int newRow, int newColumn) {
		// If cell presently selected matches new selected cell, do nothing
		if (selectedCellRow != newRow || selectedCellColumn != newColumn) {
			// Unselect cell presently selected if there is any
			if (selectedCellRow != -1) {
				int blockXIndex = selectedCellRow / SudokuGrid.BLOCK_LENGTH;
				int blockYIndex = selectedCellColumn / SudokuGrid.BLOCK_LENGTH;
				blocks[blockXIndex][blockYIndex].deselectCell(selectedCellRow, selectedCellColumn);
			}
			selectedCellRow = newRow;
			selectedCellColumn = newColumn;
		}
	}
	
	
}

package view;

import controller.GameController;

/**
 * Interface with the expected methods for the class that
 * finally implements the game view.
 * @author jmalvarez
 *
 */
public interface GameView {

	public void showMessage(String m);
	public void attachController(GameController gc);
}

package model;

/**
 * A class to represent exceptions for a number out of range.
 * @version 1.0, 31/07/21
 * @author Jose M. Alvarez Palomo
 */

public class NumberOutOfRangeException extends RuntimeException
{
    /**
	 * Default serial version id.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor.
	 * @param msg Explanatory text about the reason why the exception was raised.
	 */
	public NumberOutOfRangeException(String msg) {
       super(msg);
    }
}
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.Random;

/**
 * A class to represent a sudoku grid made of 9 x 9 cells.
 * 
 * @version 1.0, 31/07/21
 * @author Jose M. Alvarez Palomo
 */

public class SudokuGrid {

	public static final int GRID_LENGTH = 9;
	public static final int BLOCK_LENGTH = 3;

	Cell grid[][];

	/**
	 * Default constructor method. It creates a sudoku grid with all cells empty.
	 */
	public SudokuGrid() {
		grid = new Cell[GRID_LENGTH][GRID_LENGTH];
		for (int i = 0; i < GRID_LENGTH; i++) {
			for (int j = 0; j < GRID_LENGTH; j++) {
				grid[i][j] = new Cell();
			}
		}
	}

	/**
	 * Set the value num at cell (x, y) in the grid
	 * @param num
	 * @param x
	 * @param y
	 * @throws NumberOutOfRangeException
	 * @throws IndexOutOfBoundsException
	 */
	public void setValueAt(int num, int x, int y) throws NumberOutOfRangeException, IndexOutOfBoundsException {
		if (x < 0 || GRID_LENGTH <= x) {
			throw new IndexOutOfBoundsException("Invalid value for row " + x);
		} else if (y < 0 || GRID_LENGTH <= y) {
			throw new IndexOutOfBoundsException("Invalid value for column " + y);
		}
		grid[x][y].setValue(num);
	}

	/**
	 * Fix the value num at cell (x, y) in the grid
	 * @param num
	 * @param x
	 * @param y
	 * @throws NumberOutOfRangeException
	 * @throws IndexOutOfBoundsException
	 */
	public void fixValueAt(int num, int x, int y) throws NumberOutOfRangeException, IndexOutOfBoundsException {
		if (x < 0 || GRID_LENGTH <= x) {
			throw new IndexOutOfBoundsException("Invalid value for row " + x);
		} else if (y < 0 || GRID_LENGTH <= y) {
			throw new IndexOutOfBoundsException("Invalid value for column " + y);
		}
		grid[x][y].fixValue(num);
	}

	/**
	 * Get the value of cell (x, y) in the grid
	 * @param num
	 * @param x
	 * @param y
	 * @throws NumberOutOfRangeException
	 * @throws IndexOutOfBoundsException
	 */
	public int getValueAt(int x, int y) throws NumberOutOfRangeException, IndexOutOfBoundsException {
		if (x < 0 || GRID_LENGTH <= x) {
			throw new IndexOutOfBoundsException("Invalid value for row " + x);
		} else if (y < 0 || GRID_LENGTH <= y) {
			throw new IndexOutOfBoundsException("Invalid value for column " + y);
		}
		return grid[x][y].getValue();
	}

	/**
	 * Empty cell (x, y) in the grid
	 * @param num
	 * @param x
	 * @param y
	 * @throws NumberOutOfRangeException
	 * @throws IndexOutOfBoundsException
	 */

	public void emptyCell(int x, int y) throws NumberOutOfRangeException, IndexOutOfBoundsException {
		if (x < 0 || GRID_LENGTH <= x) {
			throw new IndexOutOfBoundsException("Invalid value for row " + x);
		} else if (y < 0 || GRID_LENGTH <= y) {
			throw new IndexOutOfBoundsException("Invalid value for column " + y);
		}
		grid[x][y].empty();
	}

	/**
	 * Returns state of cell (x, y) in the grid
	 * @param num
	 * @param x
	 * @param y
	 * @throws NumberOutOfRangeException
	 * @throws IndexOutOfBoundsException
	 * @return Cell state.
	 */
	public CellState cellState(int x, int y) throws NumberOutOfRangeException, IndexOutOfBoundsException {
		if (x < 0 || GRID_LENGTH <= x) {
			throw new IndexOutOfBoundsException("Invalid value for row " + x);
		} else if (y < 0 || GRID_LENGTH <= y) {
			throw new IndexOutOfBoundsException("Invalid value for column " + y);
		}
		return grid[x][y].getState();
	}

	/**
	 * Method to check if a number is different from all the values already set in
	 * an rectangular sector defined by (xmin, ymin), (xmax, ymax).
	 * 
	 * @param num.  Value to be checked
	 * @param xmin. X value of upper left corner of the sector.
	 * @param ymin. Y value of upper left corner of the sector.
	 * @param xmax. X value of lower right corner of the sector.
	 * @param ymax. Y value of lower right corner of the sector.
	 * @return true if number is different than every value in the sector, false
	 *         otherwise.
	 */
	protected int timesInSector(int num, int xmin, int ymin, int xmax, int ymax) {
		int times = 0;

		for (int i = xmin; i <= xmax; i++) {
			for (int j = ymin; j <= ymax; j++) {
				if (grid[i][j].getState() != CellState.EMPTY && num == grid[i][j].getValue()) {
					times++;
				}
			}
		}

		return times;

	}

	/**
	 * Method to check if the value set in a cell is repeated in its row, column or block.
	 * 
	 * @param row. Cell row.
	 * @param column. Cell column.
	 * @return true if number is not repeated in row, column or block, false otherwise.
	 */
	public boolean canFit(int row, int column) {
		
		return grid[row][column].getState() == CellState.EMPTY ||
				(
				// no repeated values in row
				timesInSector(grid[row][column].getValue(), row, 0, row, GRID_LENGTH - 1) <= 1 &&
				// no repeated values in column
						timesInSector(grid[row][column].getValue(), 0, column, GRID_LENGTH - 1, column) <= 1 &&
						// no repeated values in block
						timesInSector(grid[row][column].getValue(), 
								(row     / BLOCK_LENGTH) * BLOCK_LENGTH,
								(column  / BLOCK_LENGTH) * BLOCK_LENGTH, 
								((row    / BLOCK_LENGTH) * BLOCK_LENGTH) + 2,
								((column / BLOCK_LENGTH) * BLOCK_LENGTH) + 2) <= 1
				);		
	}
	
	/**
	 * Method to check whether a (partially) filled sudoku grid is valid or not
	 * 
	 * @return true if it is valid, false otherwise.
	 */
	public boolean isValid() {
		boolean valid = true;
		int i, j;
		i = 0;
		while (i < GRID_LENGTH && valid) {
			j = 0;
			while (j < GRID_LENGTH && valid) {
				if (grid[i][j].getState() != CellState.EMPTY) {
					valid = // no repeated values in row
							timesInSector(grid[i][j].getValue(), i, 0, i, GRID_LENGTH - 1) <= 1 &&
							// no repeated values in column
									timesInSector(grid[i][j].getValue(), 0, j, GRID_LENGTH - 1, j) <= 1 &&
									// no repeated values in block
									timesInSector(grid[i][j].getValue(), 
											(i / BLOCK_LENGTH) * BLOCK_LENGTH,
											(j / BLOCK_LENGTH) * BLOCK_LENGTH, 
											((i / BLOCK_LENGTH) * BLOCK_LENGTH) + 2,
											((j / BLOCK_LENGTH) * BLOCK_LENGTH) + 2) <= 1;
				}
				j++;
			}
			i++;
		}
		return valid;
	}

	/**
	 * Method to check whether a sudoku grid is full or not
	 * 
	 * @return true if it is full, false otherwise.
	 */
	public boolean isFull() {
		boolean full = true;
		int i, j;
		i = 0;
		while (i < GRID_LENGTH && full) {
			j = 0;
			while (j < GRID_LENGTH && full) {
				full = grid[i][j].getState() != CellState.EMPTY;
				j++;
			}
			i++;
		}
		return full;
	}

	/**
	 * Method to fill a (partially) filled sudoku grid
	 * 
	 * @return true if there is at least a feasible solution, false otherwise.
	 */
	public int fillSudokuGrid(boolean stopAtFirstSolution) {
		return fillSudokuGridRec(stopAtFirstSolution, 0, 0);
	}

	/**
	 * Auxiliary recursive method to fill a sudoku grid. First call must me made
	 * with x = 0, y = 0. Next calls are made following the columns of a row, then
	 * passing to the next row.
	 * 
	 * @param stopAtFirstSolution. If true, function stops after finding the first
	 *                             solution. Grid remains filled. If false, function
	 *                             finds all possible solutions. Grid finishes with
	 *                             the same values it started with, as backtrack
	 *                             applies.
	 * @param x                    Starting row.
	 * @param y                    Starting column.
	 * @return true if there is at least a feasible solution, false otherwise.
	 */
	protected int fillSudokuGridRec(boolean stopAtFirstSolution, int x, int y) {

		int solutionsFound = 0;

		// All cells already set. Finish with success.
		if ((GRID_LENGTH <= x) || ((GRID_LENGTH - 1 == x) && (GRID_LENGTH <= y))) {
			solutionsFound = 1;
		}
		// At least one cell left to check
		else {
			// We will assume that already filled cells are correctly placed.
			if (grid[x][y].getState() != CellState.EMPTY) {
				// Recursively try to fill the grid from the next position:
				// If not at the end of the row -> same row, next column.
				// Else, next row, first column
				solutionsFound += fillSudokuGridRec(stopAtFirstSolution, x + (y / (GRID_LENGTH - 1)),
						(y + 1) % GRID_LENGTH);
			}
			// Position (x, y) is empty
			else {
				ArrayList<Integer> listOfCandidates = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
//				ArrayList<Integer> listOfCandidates = randomSequence();
				removeInvalidCandidates(listOfCandidates, x, y);
				while ((solutionsFound <= 0 || !stopAtFirstSolution) && 0 < listOfCandidates.size()) {
					// looking for a valid value from 1 to 9
					int candidate = listOfCandidates.get(0);
					setValueAt(candidate, x, y);
					// Recursively try to fill the grid from the next position:
					// If not at the end of the row -> same row, next column.
					// Else -> next row, first column
					solutionsFound += fillSudokuGridRec(stopAtFirstSolution, x + (y / (GRID_LENGTH - 1)),
							(y + 1) % GRID_LENGTH);
					// If no solution found with that value, back track
					if (solutionsFound <= 0 || !stopAtFirstSolution) {
						// remove last value and try with the next one
						emptyCell(x, y);
						listOfCandidates.remove(0);
					}
				}
			}
		}
		return solutionsFound;
	}
	
	/**
	 * Generates a fresh list of candidates with values in [1..9] randomly sorted.
	 * @return Fresh list of candidates with values in [1..9] randomly sorted.
	 */
	public static ArrayList<Integer> randomSequence() {
		ArrayList<Integer> randomSeq = new ArrayList<Integer>();
		Random rand = new Random();
		while (randomSeq.size() < GRID_LENGTH) {
			int nextCandidate = rand.nextInt(GRID_LENGTH) + 1;
			if (!randomSeq.contains(Integer.valueOf(nextCandidate))) {
				randomSeq.add(Integer.valueOf(nextCandidate));
			}
		}
		return randomSeq;
	}

	/**
	 * Remove from list of candidates those already present in row, column or block
	 * 
	 * @param listOfCandidates
	 * @param x.               X coordinate in grid.
	 * @param y.               Y coordinate in grid.
	 */
	protected void removeInvalidCandidates(ArrayList<Integer> listOfCandidates, int x, int y) {
		ListIterator<Integer> candidateIt = listOfCandidates.listIterator();
		while (candidateIt.hasNext()) {
			Integer candidate = candidateIt.next();
			if (// candidate already present in row
			0 < timesInSector(candidate, x, 0, x, GRID_LENGTH - 1) ||
			// candidate already present in column
					0 < timesInSector(candidate, 0, y, GRID_LENGTH - 1, y) ||
					// candidate already present in block
					0 < timesInSector(candidate, (x / BLOCK_LENGTH) * BLOCK_LENGTH, (y / BLOCK_LENGTH) * BLOCK_LENGTH,
							((x / BLOCK_LENGTH) * BLOCK_LENGTH) + 2, ((y / BLOCK_LENGTH) * BLOCK_LENGTH) + 2)) {
				candidateIt.remove();
			}
		}
	}

	public static SudokuGrid createNewGame() {
		
		SudokuGrid sudokuGR = new SudokuGrid();

		
		// Generate a full grid
		int numberOfSolutions = sudokuGR.fillSudokuGrid(true);
		
		// Empty cells from grid until the grid has more than one possible solution.
		Random rand = new Random();
		int value = 0, posX = 0, posY = 0;
		while (numberOfSolutions < 2) {
			do {
				posX = rand.nextInt(SudokuGrid.GRID_LENGTH);
				posY = rand.nextInt(SudokuGrid.GRID_LENGTH);

			} while(sudokuGR.cellState(posX, posY) == CellState.EMPTY);
			value = sudokuGR.getValueAt(posX, posY);
			sudokuGR.emptyCell(posX, posY);
			numberOfSolutions = sudokuGR.fillSudokuGrid(false);
		}
				
		// Set back last value removed so the grid has a single solution
		sudokuGR.setValueAt(value, posX, posY);
		numberOfSolutions = sudokuGR.fillSudokuGrid(false);
		
		// Mark as FIXED cells that are initially filled
		for (int i = 0; i < GRID_LENGTH; i++) {
			for (int j = 0; j < GRID_LENGTH; j++) {
				if (sudokuGR.cellState(i, j) != CellState.EMPTY) {
					sudokuGR.fixValueAt(sudokuGR.getValueAt(i, j), i, j);
				}
			}
		}

		return sudokuGR;
		
	}
	
	
	/**
	 * Grid translated into a String. Mostly to be shown on console or text files.
	 */
	public String toString() {
		StringBuilder sudokuGridAsString = new StringBuilder();

		for (int i = 0; i < GRID_LENGTH; i++) {
			sudokuGridAsString.append(line(i) + "\n");
			for (int j = 0; j < GRID_LENGTH; j++) {
				if (j % BLOCK_LENGTH == 0) {
					sudokuGridAsString.append("# ");
				} else {
					sudokuGridAsString.append("| ");
				}
				if (grid[i][j].getState() == CellState.EMPTY) {
					sudokuGridAsString.append(" ");
				} else {
					sudokuGridAsString.append(grid[i][j].getValue());
				}
				sudokuGridAsString.append(" ");
			}
			sudokuGridAsString.append("#\n");
		}
		sudokuGridAsString.append(line(GRID_LENGTH) + "\n");
		return sudokuGridAsString.toString();
	}

	/**
	 * Returns a string with a grid line. Double if block boundary, simple otherwise
	 * @param i
	 * @return
	 */
	protected StringBuilder line(int i) {
		StringBuilder lineAsString = new StringBuilder();
		int j;

		if (i % BLOCK_LENGTH == 0) {
			lineAsString.append("=");
		} else {
			lineAsString.append("-");
		}
		for (j = 0; j < GRID_LENGTH; j++) {
			if (i % BLOCK_LENGTH == 0) {
				lineAsString.append("====");
			} else {
				lineAsString.append("----");
			}
		}
		return lineAsString;
	}
}

package model;

/**
 * A class to represent a sudoku cell.
 * Each cell may be empty or filled with a value 1 <= v < = 9 
 * @version 1.0, 31/07/21
 * @author Jose M. Alvarez Palomo
 */

public class Cell {
	private CellState state;
	private int value;

	/** 
	 * Public default constructor method.
	 * It creates an empty cell.
	 */
	public Cell() {
		empty();
	}

	/** 
	 * Constructor method with init value.
	 * @param v. Value to be set in the cell.
	 * @throws NumberOutOfRangeException
	 */
	public Cell(int v) throws NumberOutOfRangeException {
		setValue(v);
	}

	/**
	 * Method to get the value of a cell.
	 * @return Cell value.
	 * @throws EmptyCellException
	 */
	public int getValue() throws EmptyCellException{
		if (this.getState() == CellState.EMPTY) {
			throw new EmptyCellException("Empty cell. Cannot get value");
		} 
		return value; 
	}

	/**
	 * Method to get the cell state.
	 * @return Cell state.
	 */
	public CellState getState() {
		return state;
	}

	/**
	 * Method to empty a cell
	 */
	public void empty() {
		state = CellState.EMPTY;
		value = 0;
	}

	/** 
	 * Method to set a value in a cell.
	 * @param v. Value to be set in the cell.
	 * @throws NumberOutOfRangeException
	 */
	public void setValue(int v) throws  NumberOutOfRangeException{
		if (1 <= v && v <= 9) {
			state = CellState.FILLED;
			value = v;
		} else {
			throw new NumberOutOfRangeException("Value in a sudoku cell must be between 1 and 9.");
		}
	}

	/** 
	 * Method to set a value in a cell.
	 * @param v. Value to be set in the cell.
	 * @throws NumberOutOfRangeException
	 */
	public void fixValue(int v) throws  NumberOutOfRangeException{
		if (1 <= v && v <= 9) {
			state = CellState.FIXED;
			value = v;
		} else {
			throw new NumberOutOfRangeException("Value in a sudoku cell must be between 1 and 9.");
		}
	}
}

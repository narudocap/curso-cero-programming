package model;

/**
 * A class to represent exceptions for an empty cell.
 * This exception is raised when it is executed the getValue()
 * method on an empty cell.
 * @version 1.0, 31/07/21
 * @author Jose M. Alvarez Palomo
 */

public class EmptyCellException extends RuntimeException{

	/**
	 * Default serial version id.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default constructor.
	 * @param msg explanatory text about the reason why the exception was raised.
	 */
	public EmptyCellException(String msg) {
       super(msg);
    }

}

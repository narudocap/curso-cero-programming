package model;

/**
 * FIXED: The cell value was set before the game started. The player is not able
 * to change its value.
 * 
 * EMPTY: The cell value is not set. The player can set a value to this cell
 * during the game.
 * 
 * FILLED: The cell has been set a value. The player can change the value or
 * empty the cell.
 * 
 * @author jmalvarez
 *
 */
public enum CellState {
	FIXED, EMPTY, FILLED;
}

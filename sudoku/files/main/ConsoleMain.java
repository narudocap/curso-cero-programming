package main;

import controller.GameController;
import model.SudokuGrid;
import view.ConsoleView;

public class ConsoleMain {

	SudokuGrid sudoku = new SudokuGrid();
	
	public static void main (String args[]) {

		SudokuGrid sudoku = SudokuGrid.createNewGame();

		sudoku.fillSudokuGrid(false);
		
		ConsoleView cView = new ConsoleView(sudoku);
		
		GameController gControl = new GameController(sudoku, cView);
		
		cView.attachController(gControl);
		
		cView.play();

	}

}

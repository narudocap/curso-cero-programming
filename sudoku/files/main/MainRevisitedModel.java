package main;

import model.SudokuGrid;

public class MainRevisitedModel {

	public static void printValid(SudokuGrid sd) {
		if (sd.isValid()) {
			System.out.println("The partially built sudoku is valid\n");
		}else {
			System.out.println("The partially built sudoku is not valid\n");
		}
	}

	public static void printFull(SudokuGrid sd) {
		if (sd.isFull()) {
			System.out.println("The sudoku is full\n");
		}else {
			System.out.println("The sudoku is not full\n");
		}
	}


	public static void main(String[] args) {
		SudokuGrid sudoGrid = new SudokuGrid();

		sudoGrid.setValueAt(4, 2, 3);
		sudoGrid.setValueAt(5, 2, 4);

		System.out.println(sudoGrid.toString());

		printValid(sudoGrid);

		sudoGrid.setValueAt(5, 0, 4);

		System.out.println(sudoGrid.toString());

		printValid(sudoGrid);

		printFull(sudoGrid);

		sudoGrid = new SudokuGrid();
		sudoGrid.fillSudokuGrid(true);

		System.out.println(sudoGrid.toString());

		printFull(sudoGrid);

	}

}

package main;

import model.SudokuGrid;

public class MainFirstVersion {

	public static void main(String[] args) {
		
		SudokuGrid sudoGrid = new SudokuGrid();
		
		sudoGrid.setValueAt(4,  2, 3);
		
		sudoGrid.setValueAt(4,  2, 1);
		
		System.out.println(sudoGrid.toString());
		
		sudoGrid.emptyCell(2, 3);
		
		System.out.println(sudoGrid.toString());

	}

}

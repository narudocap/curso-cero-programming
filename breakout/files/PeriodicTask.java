package breakout;

import java.util.Timer;
import java.util.TimerTask;

/**
 * The state of the game is updated periodically. A scheduled task is in charge
 * of moving the ball and the paddle, and checking the possible collisions.
 */
public class PeriodicTask  {
	/**
	 * The model instance variable keeps a reference to the model of the MVC
	 * pattern. Every time the task gets activated, the paddle and ball are moved,
	 * and potential collisions are checked.
	 */
	private GameModel model;
	/**
	 * The view instance variable keeps a reference to the view of the MVC pattern.
	 * Every time the task gets activated, the view is repainted.
	 */
	private GameView view;
	/**
	 * A timer object is set to periodically activate the task.
	 */
	private Timer timer;
	/**
	 *  time between two states
	 */
	private int period=10; 

	/**
	 * If debugMode==true the ball never falls.
	 */
	private boolean debugMode;

	public PeriodicTask(GameModel m, GameView v) {
		debugMode=true;
		model = m;
		view = v;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		startTimer();
	}
	
	private void startTimer() {
		// The timer is set to run the TimerTask object (this) every 10 milliseconds
		// which, in turn, will call executeStep().
	    timer = new Timer();
	    timer.scheduleAtFixedRate(new TimerTask() {
	        public void run() {
	            executeStep();
	        }
	    }, 0, period);
	}
	
	/**
	 * The timer task invokes this method. It performs the periodic tasks, 
	 * namely moving the ball, the paddle, checking for collisions, and repainting
	 * the GUI.
	 */
	public void executeStep() {
		model.getBall().move();
		model.getPaddle().move();
		if (model.getBricks().size() == 0) {
			model.gameOver(true);
			stopGame("You win!");
		} else if (model.getBall().getRectangle().getMinY() > GamePanel.BOTTOM) {
			model.gameOver(true);
			stopGame("Game Over!");
		} else {
			model.checkCollision();
		}
		view.updateScore(model.getScore());
		view.updateStage(model.getStage());
		view.repaint();
	}

	/**
	 * When the game is over, the stopGame method will notify the model, cancel the
	 * periodic timer, and set the final message.
	 */
	public void stopGame(String msg) {
		timer.cancel();
		view.setMessage(msg);
	}
}

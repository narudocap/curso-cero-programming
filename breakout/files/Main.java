import breakout.PeriodicTask;
import breakout.Breakout;
import breakout.GameFrame;
import breakout.GameModel;
import breakout.GameView;
import breakout.PaddleController;

public class Main {
	public static void main(String[] args) {
		GameModel model = new Breakout();
		PaddleController paddleCtrl = new PaddleController(model);
		GameView view = new GameFrame("Breakout", model);
		view.addKeyListener(paddleCtrl);
		new PeriodicTask(model, view);
	}
}
package breakout;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * The PaddleController class is a key listener. The PaddleController class
 * extends the KeyAdapter class, which implements the KeyListener interface. The
 * KeyAdapter class provides default implementations for all the methods in the
 * KeyListener interface, so that we only need to provide re-definitions for
 * those methods we need. In the case of the PaddleController we only want to
 * act on the pressing and releasing of the left and right cursor keys.
 */
public class PaddleController extends KeyAdapter {
	/**
	 * The model instance variable keeps a reference to the model of the MVC
	 * pattern. If a key is pressed or released, the controller acts on the model,
	 * changing the direction of the paddle.
	 */
	private GameModel model;

	/**
	 * When a PaddleController is created we just save the reference to the model is
	 * the corresponding variable.
	 *
	 * @param m model of the MVC pattern
	 */
	public PaddleController(GameModel m) {
		model = m;
	}

	/**
	 * When a key is pressed, a keyPressed message is sent to all key listeners. The
	 * keyPressed message carries a KeyEvent object, which has information on the
	 * pressed key. If the left or right cursor keys are pressed, the direction of
	 * the paddle is changed correspondingly. If the key pressed is any other key,
	 * nothing is done.
	 *
	 * @param e KeyEvent object representing the key-pressing event
	 */
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			model.getPaddle().setDirection(-1); // moving to the left
		} else if (key == KeyEvent.VK_RIGHT) {
			// TO BE COMPLETED
		  // moving to the right
		}
	}

	/**
	 * When a key is released, a keyReleased message is sent to all key listeners.
	 * The keyReleased message carries a KeyEvent object, which has information on
	 * the released key. If the released key is the left or right cursor, the
	 * direction of the paddle is set to 0. If the key pressed is any other key,
	 * nothing is done.
	 *
	 * @param e KeyEvent object representing the key-releasing event
	 */
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
			// TO BE COMPLETED
		  // stopped
		}
	}

}

import breakout.Breakout;
import breakout.GameFrame;
import breakout.GameModel;
import breakout.GameView;

public class MainView {
	public static void main(String[] args) throws InterruptedException {
		GameModel model = new Breakout();
		GameView view = new GameFrame("Breakout", model);
		while (!model.gameOver()) {
			view.repaint();
			model.getBall().move();
			if (model.getBricks().size() == 0) {
				model.gameOver(true);
				view.setMessage("You win!");
			} else if (model.getBall().getRectangle().getMinY() > 390) {
				model.gameOver(true);
				view.setMessage("Game over!");
			} else {
				model.checkCollision();
			}
			Thread.sleep(10);
		}
	}
}

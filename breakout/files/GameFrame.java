package breakout;

import javax.swing.JFrame;

/**
 * GameFrame is a JFrame that implements the GameView interface. It implements
 * the view component of the implementation of the Breakout game.
 */
public class GameFrame extends JFrame implements GameView {
	private static final long serialVersionUID = -103955242750526115L;

	public GameFrame(String title, GameModel model) {
		super("Breakout");
		setContentPane(new MainPanel(model));
		pack();
		setFocusable(true);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void setMessage(String msg) {
		((MainPanel) getContentPane()).setMessage(msg);
	}

	@Override
	public void updateScore(int score) {
		((MainPanel) getContentPane()).updateScore(score);
	}
	
	@Override
	public void updateStage(int stage) {
		((MainPanel) getContentPane()).updateStage(stage);
		
	}
}
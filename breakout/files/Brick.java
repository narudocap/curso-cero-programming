package breakout;

/**
 * Bricks are passive objects, it is the ball that detects the collision and
 * then destroy the brick.
 */
public class Brick extends Sprite {
	/**
	 * A brick is created in the position passed as argument, and using the image in
	 * the file to visualize it.
	 * 
	 * @param nx   is the initial value for the x coordinate
	 * @param ny   is the initial value for the y coordinate
	 * @param file is the file that contains the image to represent the
	 *             visualization of the sprite
	 */
	public Brick(int nx, int ny, String file) {
		super(nx, ny, file);
	}
}
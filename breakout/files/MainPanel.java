package breakout;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainPanel extends JPanel {
	private static final long serialVersionUID = -60766456762898735L;
	private JTextField score;
	private JTextField stage;
	private GamePanel actionPanel;

	/**
	 * Border-layout panel showing the current score at the north, and the action
	 * panel in its center. It gets a reference to the model to be able to get the
	 * positions of the objects from the action panel for their display.
	 * 
	 * @param model GameModel object to be visualized
	 */
	public MainPanel(GameModel model) {
		setLayout(new BorderLayout());
		// top
		JPanel top = new JPanel();
		top.add(new JLabel("Score: "));
		score = new JTextField("0", 5);
		top.add(score);
		top.add(new JLabel("Stage: "));
		stage = new JTextField("0", 5);
		top.add(stage);
		add(top, BorderLayout.NORTH);
		// center
		actionPanel = new GamePanel(model);
		add(actionPanel, BorderLayout.CENTER);
	}

	/**
	 * Sets the message to print upon the finalization of the game (depending on
	 * whether the player wins or loses).
	 * 
	 * @param msg Message to be printed.
	 */
	public void setMessage(String msg) {
		actionPanel.setMessage(msg);
	}

	/**
	 * Updates the shown score.
	 * 
	 * @param score Value to be depicted.
	 */
	public void updateScore(int s) {
		score.setText(Integer.toString(s));
	}
	/**
	 * Updates the shown stage.
	 * 
	 * @param stage Value to be updated.
	 */
	public void updateStage(int st) {
		stage.setText(Integer.toString(st));
		
	}
}